# 使用說明
**內部預設之發送陣列為post_data**<br>
**開始使用前請先進入receive.php中修改您的Access Token**
### 一、傳送訊息
使用以下語法 `$post_data = sendText($post_data,"要傳送的字串")`。
### 二、傳送貼圖
使用以下語法 `$post_data = sendSticker($post_data,"PackageId","StickerId")`。<br>
可參考 [Line 貼圖官方說明](https://developers.line.me/media/messaging-api/messages/sticker_list.pdf)。
### 三、傳送圖片
使用以下語法 `$post_data = sendImage($post_data,"實際圖片網址","預覽圖片網址")`。
### 四、傳送影片
使用以下語法 `$post_data = sendVideo($post_data,"實體影片網址","預覽圖片網址")`。
### 五、傳送音訊
使用以下語法 `$post_data = sendAudio($post_data,"實體音訊網址","音訊長度(百萬分之一秒)")`。
### 六、傳送快速回覆(本範例皆使用`$quick`變數，可自行更換變數名稱)
##### （一）初始化一個變數
請使用以下語法<br>
`$quick = new_quick_reply()`
####  （二）設定帶入字串
請使用下列語法<br>
`$quick = setup_quick_reply("要帶入的字串")`
####  （三）新增快速回覆
請輸入下列語法<br>
`$quick = push_message_quick_reply($quick,"圖片網址(若沒有即帶空值)","顯示的字串","點擊下去使用者回覆的字串")`
### 七、傳送地理位置
使用以下語法 `$post_data = sendLocation($post_data,"標題","地址","緯度","經度")`。
### 八、更新Rich Menu(本範例皆使用 `$rich_menu` 變數，可自行更換變數名稱)
#### （一）初始化一個Rich Menu
使用這個語法新增一個空的Rich Menu `$rich_menu = new_rich_menu();`<br>
使用這個語法設定這個Rich Menu的Size `$rich_menu = setup_rich_menu_size($rich_menu,寬度[Int],高度[Int]);`<br>
再使用這個語法設定這個Rich Menu的資訊 `$rich_menu = setup_rich_menu_detail($rich_menu,"這個Rich Menu的名字","顯示在聊天視窗底部的文字");`<br><br>
#### （二）設定每個區域的位置及動作
1. 傳送訊息：<br>
`$rich_menu = push_message_rich_menu($rich_menu,X軸座標[Int],Y軸座標[Int],寬度範圍[Int],高度範圍[Int],"Label值[可留空值]","用戶要傳送的訊息");`<br>
2. 傳送PostBack：<br>
`$rich_menu = push_postback_rich_menu($rich_menu,X軸座標[Int],Y軸座標[Int],寬度範圍[Int],高度範圍[Int],"Label值[可留空值]","用戶要傳送的訊息[可留空]","Postback資料");`
3. 開啟連結：<br>
`$rich_menu = push_uri_rich_menu($rich_menu,X軸座標[Int],Y軸座標[Int],寬度範圍[Int],高度範圍[Int],"Label值[可留空值]","要開啟的連結網址");`<br>
4. 開啟日期選擇器(回傳PostBack)：<br>
`$rich_menu = push_datepicker_rich_menu($rich_menu,X軸座標[Int],Y軸座標[Int],寬度範圍[Int],高度範圍[Int],"Label值[可留空值]","一起回傳的內容","起始日期","截止日期");`<br>
5. 開啟時間選擇器(回傳PostBack)：<br>
`$rich_menu = push_timepicker_rich_menu($rich_menu,X軸座標[Int],Y軸座標[Int],寬度範圍[Int],高度範圍[Int],"Label值[可留空值]","一起回傳的內容","起始時間","截止時間");`<br>
6. 開啟日期時間選擇器(回傳PostBack)：<br>
`$rich_menu = push_datetimepicker_rich_menu($rich_menu,X軸座標[Int],Y軸座標[Int],寬度範圍[Int],高度範圍[Int],"Label值[可留空值]","一起回傳的內容","起始日期","起始時間","截止日期","截止時間");`<br>
7. 開啟相機傳送圖片：<br>
`$rich_menu = push_camera_rich_menu($rich_menu,X軸座標[Int],Y軸座標[Int],寬度範圍[Int],高度範圍[Int],"Label值[可留空值]");`<br>
8. 開啟地理位置選擇器：<br>
`$rich_menu = push_location_rich_menu($rich_menu,X軸座標[Int],Y軸座標[Int],寬度範圍[Int],高度範圍[Int],"Label值[可留空值]");`<br><br>
#### （三）傳送Rich Menu
1. 上傳Rich Menu：<br>
`$rich_menu_id = send($access_token,$rich_menu);`<br>
*在這裡我們設定一個新的變數 `$rich_menu_id` 來取得 Line 給我們的ID*<br><br>
2. 上傳 Rich Menu 背景圖片：<br>
`setup_rich_menu_image($access_token,$rich_menu_id,"圖片網址或相對路徑");`<br><br>
3. 指定這個Rich Menu為預設的Rich Menu(大概是啟用的概念)：
`set_default_rich_menu_everyone($access_token,$rich_menu_id)`<br>
### 九、傳送(將整個$post_data傳送出去)
請使用下列語法傳送`send($access_token,$post_data);`。