<?php
function send($access_token,$post_data){
    $file = fopen("Line_log.txt", "a+");
    fwrite($file, json_encode($post_data)."\n");
    
    $ch = curl_init("https://api.line.me/v2/bot/message/reply");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Authorization: Bearer '.$access_token
        //'Authorization: Bearer '. TOKEN
    ));
    $result = curl_exec($ch);
    fwrite($file, $result."\n");
    fclose($file);
    curl_close($ch);
}
