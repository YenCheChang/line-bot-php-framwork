<?php
require("receive.php");
require("reply-api/text.php");
require("reply-api/sticker.php");
require("reply-api/image.php");
require("reply-api/video.php");
require("reply-api/audio.php");
require("reply-api/quick-reply.php");
require("reply-api/location.php");
require("reply-api/imagemap.php");
require("reply-api/template/button.php");
require("reply-api/template/confirm.php");
require("reply-api/rich-menu.php");
require("send.php");
$post_data = [
    "replyToken" => $reply_token,
    "messages" => [
    ]
];
